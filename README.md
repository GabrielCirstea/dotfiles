# dotfiles

Here are my dotfile, first try to put them on a git repo.

The files are managed with [chezmoi](https://www.chezmoi.io/)

This repo is just at the beginning

## Install:

Install with curl:
```
sh -c "$(curl -fsLS get.chezmoi.io)"
```

Get the dot files:
```
chezmoi init --apply https://bitbucket.org/GabrielCirstea/dotfiles.git
```

Get the vim conf:
```
git clone https://GabrielCirstea@bitbucket.org/GabrielCirstea/vimcfg.git ~/Documents/vimcfg
cd ~/Documents/vimcfg && ./install
```

Get the nvim conf:
```
git clone https://GabrielCirstea@bitbucket.org/GabrielCirstea/nvim-kickstart.git ~/.config/nvim
```


### util scrips

Get the scripts directory

```
git clone https://gitlab.com/GabrielCirstea/utilscripts.git ~/Scripts
```

## Structure

The name of the file may start with "dot_" instead of the character '.'
The executables have the word "executable_" prepend to the name

Seems that chezmoi is structuring the repo easy to understand and set manually
