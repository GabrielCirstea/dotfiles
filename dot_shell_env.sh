#!/bin/sh

# some quick env settings

# add go/bin folder to path
echo $PATH | grep -Eq "(^|:)$HOME/go/bin(:|)" || PATH=$PATH:$HOME/go/bin
export GOPATH="$HOME/go"
export GOROOT="$HOME/Documents/build/go"

export EDITOR=vim

# quick filebrowser setup - no login required
alias fbrs_quick="filebrowser -a 0.0.0.0 -p 8080 --noauth --disable-exec -d /tmp/filebrowser.db"

# $1 - file path
share_null()
{
	file=${1?"file path required"}
	url=$(curl -F"file=@$file" http://0x0.st)
	echo "$(date) $file: $url" >> ~/.cache/share_null.log
	echo "$url"
}

# use filebrowser locally
# $1 - username
# $2 - password
# $3 - root dir to share
fbrs_local()
{
	username=${1?"MIssing username - \$1"}
	password=${2?"Missing password - \$2"}
	root=${3-"."}

	# clean the database first
	[ -f /tmp/filebrowser.db ] && rm /tmp/filebrowser.db
	passH=$(filebrowser hash "$password")
	filebrowser -a 0.0.0.0 -p 8080 -d /tmp/filebrowser.db \
		--username "$username" \
		--password "$passH" \
		--root "$root" \
		--disable-exec

	echo "Remove database"
	rm /tmp/filebrowser.db
}

# lf stuff
LFCD="$HOME/.config/lf/lfcd.sh"
if [ -f "$LFCD" ]; then
    source "$LFCD"
fi

bind '"\C-o":"lfcd\C-m"'  # bash

# fzf stuff
. ~/.config/fzf/completion.bash
. ~/.config/fzf/key-bindings.bash
