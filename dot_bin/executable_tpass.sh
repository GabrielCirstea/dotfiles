#!/bin/sh

# list the passwords from pass
echo $PATH | grep -Eq "(^|:)$HOME/bin(:|)" || PATH=$PATH:$HOME/bin
if [ -z "$TMUX" ]
then
	echo "This script must run inside tmux"
	exit 1
fi

# check if fzf is installed
if ! command -v fzf > /dev/null
then
	echo "ERROR: fzf not found"
	exit 1
fi

#select=$(pass | tail -n+2 | cut -d" " -f2 | fzf)
PASSDIR="$HOME/.password-store/"
select=$(find $PASSDIR -name "*.gpg" | sed "s|$PASSDIR\(.*\?\).gpg|\1|" | fzf)
[ -n "$select" ] &&  tmux set-buffer -b selectThing $(pass "$select")
