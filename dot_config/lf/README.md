# LF conf

## Prefixes

Mostly used when creating functions

:  read (default)  builtin/custom command
$  shell           shell command
%  shell-pipe      shell command running with the ui
!  shell-wait      shell command waiting for key press
&  shell-async     shell command running asynchronously
