# Fileberowser conf files and scripts

## Files

* function.sh - simple functions/aliases for quick server setup

## Notes

### Filebrowser config

Example of filebrowser.json

```
{
  "port": 8080,
  "noAuth": false,
  "baseURL": "/filebrowser",
  "address": "127.0.0.1",
  "alternativeReCaptcha": false,
  "reCaptchaKey": "",
  "reCaptchaSecret": "",
  "database": "/home/user/filebrowser",
  "log": "stdout",
  "plugin": "",
  "scope": ".",
  "root": "/home/user/filebrowser/cloud",
  "allowCommands": false,
  "allowEdit": true,
  "allowNew": true,
  "commands": [
    "ls",
    "df"
  ]
}
```

The config CLI allow to set values on filebrowser.db or othe .db file and no longer
require a .json conf file.

```
filebrowser config set --auth.method=json
```

To see the curent users:

```
filebrowser users ls
```

To add an user:

```
filebrowser user add <name> <password> [options]
```

If, for some reassons, there is no admin user, use option

```
--perm.admin
```

for a new user.
