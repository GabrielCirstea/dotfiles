#!/bin/sh

# quick filebrowser setup - no login required
alias fbrs_quick="filebrowser -a 0.0.0.0 -p 8080 --noauth --disable-exec -d /tmp/filebrowser.db"

# use filebrowser locally
# $1 - username
# $2 - password
# $3 - root dir to share
fbrs_local()
{
	username=${1?"MIssing username - \$1"}
	password=${2?"Missing password - \$2"}
	root=${3-"."}

	# clean the database first
	[ -f /tmp/filebrowser.db ] && rm /tmp/filebrowser.db
	passH=$(filebrowser hash "$password")
	filebrowser -a 0.0.0.0 -p 8080 -d /tmp/filebrowser.db \
		--username "$username" \
		--password "$passH" \
		--root "$root" \
		--disable-exec

	echo "Remove database"
	rm /tmp/filebrowser.db
}
